import {Pagination} from "react-bootstrap";
import 'bootstrap/dist/css/bootstrap.min.css';
import {FC, useMemo} from "react";

interface PaginationProps {
    totalCount : number,
    siblingCount : number,
    currentPage : number,
    pageSize : number,
    onPageChange : (page: number) => void,
}


const range = (start = 1, end = 1) : number[] => {
    let length: number = end - start + 1;
    return Array.from({ length }, (_, idx) => idx + start);
};

const DOTS : number = 0;

const  CustomPagination : FC<PaginationProps> = ({
                                                     totalCount = 0,
                                                     pageSize = 15,
                                                     siblingCount = 1,
                                                     currentPage = 1,
                                                     onPageChange
                                                 }) => {

    const paginationRange: number[] = useMemo(() => {
        const totalPageCount: number = Math.ceil(totalCount / pageSize);

        // Pages count is determined as siblingCount + firstPage + lastPage + currentPage + 2*DOTS
        const totalPageNumbers: number = siblingCount + 5;

        /*
          If the number of pages is less than the page numbers we want to show in our
          paginationComponent, we return the range [1..totalPageCount]
        */
        if (totalPageNumbers >= totalPageCount) {
            return range(1, totalPageCount);
        }

        const leftSiblingIndex: number = Math.max(currentPage - siblingCount, 1);
        const rightSiblingIndex: number = Math.min(
            currentPage + siblingCount,
            totalPageCount
        );

        /*
          We do not want to show dots if there is only one position left
          after/before the left/right page count as that would lead to a change if our Pagination
          component size which we do not want
        */
        const shouldShowLeftDots: boolean  = leftSiblingIndex > 2;
        const shouldShowRightDots: boolean = rightSiblingIndex < totalPageCount - 2;

        const firstPageIndex: number = 1;
        const lastPageIndex = totalPageCount;

        if (!shouldShowLeftDots && shouldShowRightDots) {
            let leftItemCount = 3 + 2 * siblingCount;
            let leftRange = range(1, leftItemCount);

            return [...leftRange, DOTS, totalPageCount];
        }

        if (shouldShowLeftDots && !shouldShowRightDots) {
            let rightItemCount = 3 + 2 * siblingCount;
            let rightRange = range(
                totalPageCount - rightItemCount + 1,
                totalPageCount
            );
            return [firstPageIndex, DOTS, ...rightRange];
        }

        if (shouldShowLeftDots && shouldShowRightDots) {
            let middleRange = range(leftSiblingIndex, rightSiblingIndex);
            return [firstPageIndex, DOTS, ...middleRange, DOTS, lastPageIndex];
        }
        return []
    }, [totalCount, pageSize, siblingCount, currentPage]);

    if (currentPage === 0 || paginationRange.length < 2) {
        return null;
    }

    const onNext = () => {
        onPageChange(currentPage + 1);
    };

    const onPrevious = () => {
        onPageChange(currentPage - 1);
    };

    let lastPage = paginationRange[paginationRange.length - 1];

    return(
        <div style={{margin: '20px'}}>
            <Pagination>
                <Pagination.Prev   onClick={onPrevious} disabled={currentPage === 1} />
                {paginationRange.map((pageNumber, index) => {
                    if (pageNumber === DOTS) {
                        return <Pagination.Ellipsis key={index}/>
                    }

                    return (<Pagination.Item key={index} onClick={() => onPageChange(pageNumber)}
                                             active={pageNumber === currentPage}>{pageNumber}</Pagination.Item>);
                })}
                <Pagination.Next onClick={onNext} disabled={currentPage === lastPage}/>
            </Pagination>
        </div>
    )
}

export default CustomPagination;