import React from 'react';
import './App.css';
import CustomPagination from "./CustomPagination/CustomPagination";

function App() {
    const onPageChange = (page: number) => {
        console.log(page)
    }
  return (
     <>
        <CustomPagination
            onPageChange={onPageChange}
            totalCount={100}
            pageSize={15}
            siblingCount={1}
            currentPage={7} />
     </>
  );
}

export default App;
